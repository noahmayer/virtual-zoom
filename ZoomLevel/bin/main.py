from ZoomLevel.core.utils.virtual_zoom import Zoom
from ZoomLevel.core.api import run_api
import threading


def main():
    # Calling the zoom instance, starting the API and the class Zoom, parallel via two threads.
    show_cam_output = True # Set to true or false for cam output or no cam output
    zoom_instance = Zoom()
    zoom_instance._show_cam_output = show_cam_output
    zoom_instance_thread = threading.Thread(target=zoom_instance.run)
    zoom_instance_thread.start() 
    api_thread = threading.Thread(target=run_api(zoom_instance))
    api_thread.start()
    zoom_instance_thread.join()
    api_thread.join()

if __name__ == "__main__":
    main()
