from setuptools import setup, find_packages


setup(
    name="ZoomLevel",
    version="0.0.0",
    author="Noah Mayer",
    packages= find_packages()
)