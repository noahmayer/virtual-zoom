Benötigte Mittel:

pip install cvzone==1.5.3
pip install opencv_python==3.4.11.43
pip install uvicorn

Python Version 3.10.5

Anleitung zum Gestengesteuerten zoomen

1. Zoomlevel Einstellen
    Geste: Beide Hände -> Daumen und Zeigefinger gestreckt 
    Werte: Zoomlevel 0-10 invertiert
        - Beginn der Geste relativ zur distanz zwischen den beiden Daumen 0
        - Umso weiter auseinander umso höheres Zoomlevel
        - Um auf das Zoomlevel 10 zu erreichen muss die Geste in der Position Daumen zu Daumen beginnen
    Anmerkung: 
        - Wenn die Hände nach dem erhöhen des Zoomlevels die Geste, ohne sie zu Fixieren, ändert wird das Zoomlevel auf 5 zurückgesetzt
    Use: Um schnell eine gewisse stelle zu überprüfen 
    
2. Fixieren des Zoomlevels
    Geste: Beide Hände -> alle Finger gestreckt
    Anmerkung: nach dem Fixieren kann im eingestellten Zoomlevel gearbeitet werden bis dieses wieder zurückgesetzt wird
    Use: Mit fixiertem Zoomlevel arbeiten 

3. Löschen des Zoomlevels
    Geste: Beide Hände -> Zeige und Ringfinger gestreckt
    Anmerkung: Nach dem Löschen des Zoomlevels wird dieses auf 5 zurückgesetzt
    Use: Zoomlevel Löschen

Notes:  
    - Die Gesten 2 und 3 können nur nach dem einstellen eines Zoomlevels bzw nach dem Fixieren eines Zoomlevels ausgeführt werden
    - Die Werte werden nur bei änderung ausgegeben -> sparen von Ressourcen
    - Die einzelnen Steps zwischen den Zoomlevels sind auf eine Distanz von ca. 60cm Empfernung zur Kamera konfiguriert, um dies Final auf den Maschienenführer abzustimmen muss das Programm in der Fahrerkabine getestet und angepasst werden, um ein optimales Ergebnis zu erzielen
