import cv2
from cvzone.HandTrackingModule import HandDetector


class Zoom:
    def __init__(self) -> None:
    # Calling up the camera, hand recognition and creating various variables.
        self._frame = cv2.VideoCapture(0)
        self._frame.set(3, 900)
        self._frame.set(4, 600)
        self._hand_detector = HandDetector(detectionCon = 0.8) # With a probability of 80% the hands are recognized.
        self._dist_start = None
        self._zoom_range = 0
        self.zoomlevel = 5
        self._fixed_value = 0
        self._gesturing = False
        self._prev_fixed_value = None
        self._prev_value = None
        self._show_cam_output = False

    def get_value_type(self) -> int:
        if self._gesturing:
            # Recognizing the fixed level by the edge of the _gesturing variable. 
            # The level may only be fixed if the current value is not the standart value (5).
            if self._fixed_value != 5 and self._fixed_value != self._prev_fixed_value:
                print("Fixiertes zoomlevel:", self._fixed_value)
                self._prev_fixed_value = self._fixed_value
            else:
                self._dist_start = None
                self._zoom_range = 0
            return self._fixed_value
        else:
            # Recognizing the current level by the edge of the _gesturing variable.
            if self.zoomlevel != self._prev_value:
                print("Aktuelles zoomlevel:", self.zoomlevel)
                self._prev_value = self.zoomlevel
            return self.zoomlevel

    def gesture_zoom(self) -> None:
        # Calculating the distance between the hands and illustrating the zoom gesture
        if self._dist_start is None:
            length, _, _ = self._hand_detector.findDistance(self._hands[1]['center'], \
                                                            self._hands[0]['center'], self._img)
            self._dist_start = length

        length, _, _ = self._hand_detector.findDistance(self._hands[1]['center'], \
                                                        self._hands[0]['center'], self._img)
        self._zoom_range = int((length - self._dist_start) // 2)
        self._hands[1]['center'][0], self._hands[1]['center'][1] 

    def gesture_fix_zoom(self) -> None:
        # If the gesture is fixed, the fixed value is adopted and the edge of the _gesturing variable is set to true.
        if self._fixed_value != 5:
            self._fixed_value = self.zoomlevel
            self._gesturing = True

    def gesture_reset_zoom(self) -> None:
        # Reset the zoomlevel to 5 and set the _gesturing variable to false.
        self._gesturing = False
        if self._prev_fixed_value != 5:
            self.zoomlevel = 5
            print("Zurückgesetztes zoomlevel:", self.zoomlevel)
            self._prev_fixed_value = self.zoomlevel

    def calculate_levels(self) -> None: 
        # Calculate the zoom levels 0 - 10 using the _zoom_range variable which contains the sensivity between the gestures.  
        if self._zoom_range == 0:
            self.zoomlevel = 5           
        elif self._zoom_range > 0:
            self.zoomlevel = min(5, max(0, 5 - int(self._zoom_range / 16)))           
        elif self._zoom_range < 0:
            self.zoomlevel = max(5, min(10, 5 + int( abs(self._zoom_range) / 16)))           

    def get_zoom (self) -> int:
        # Includes the three gestures (zoom, lock, reset).
        # It is also declared that the unfixed zoom level goes to 5 if there are no hands in the camera frame.
        # This function returns the zoomlevel       
        while(1):
            _, self._img = self._frame.read()
            self._hands, self._img = self._hand_detector.findHands(self._img)


            if len(self._hands) == 2:
                # Gesture to zoom the level
                if self._hand_detector.fingersUp(self._hands[1]) == [1, 1, 0, 0, 0] \
                    and self._hand_detector.fingersUp(self._hands[0]) == [1, 1, 0, 0, 0]:
                    self._hands[1]
                    self._hands[0]
                    self.gesture_zoom()  
                # Gesture to lock the level                            
                if self.zoomlevel != 5:
                    if self._hand_detector.fingersUp(self._hands[0]) == [1, 1, 1, 1, 1] \
                        and self._hand_detector.fingersUp(self._hands[1]) == [1, 1, 1, 1, 1]:
                        self.gesture_fix_zoom() 
                # Gesture to reset the level                               
                if self._hand_detector.fingersUp(self._hands[0]) == [0, 1, 1, 0, 0] \
                    and self._hand_detector.fingersUp(self._hands[1]) == [0, 1, 1, 0, 0]:    
                    self.gesture_reset_zoom()              
            else:
                self._dist_start = None
                self._zoom_range = 0

            self.calculate_levels()
            self.get_value_type()

            if self._show_cam_output:
                cv2.imshow('output', self._img)

            cv2.waitKey(int(1/60 * 1000))

    def run(self) -> None:
        print("Hello")
        self.get_zoom()

