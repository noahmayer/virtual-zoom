from fastapi import FastAPI, BackgroundTasks
import uvicorn


app = FastAPI()
zoom_instance = None

@app.put("/set_zoom/{new_zoomlevel}")
# Transferring the current zoom level to the api
async def set_zoom(new_zoomlevel: int, background_tasks: BackgroundTasks):
    zoom_instance.zoomlevel = new_zoomlevel
    background_tasks.add_task(send_notification, new_zoomlevel)
    return {"message": "Zoomlevel erfolgreich aktualisiert", "new_zoomlevel": new_zoomlevel}

@app.get("/get_zoom")
# For the bash command below to check the current level
async def get_zoom():
    return {"Aktuelles zoomlevel": zoom_instance.get_value_type()}

def send_notification(new_zoomlevel: int):
     print(f"Neues Zoomlevel: {new_zoomlevel}")

def run_api(zoom):
    # Create an instance of the zoom class and start fastapi automatically
    global zoom_instance
    zoom_instance = zoom
    uvicorn.run(app=app, host="127.0.0.1", port=8000)
